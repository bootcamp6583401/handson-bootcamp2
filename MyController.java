import java.text.NumberFormat;
import java.util.Locale;

class MyController {
    public static void main(String[] args) {
        int buyPrice;
        double tax;
        int comission;
        int distance;

        if (args.length > 2) {
            try {
                buyPrice = Integer.parseInt(args[0]);
                tax = Double.parseDouble(args[1]);
                comission = Integer.parseInt(args[2]);
                distance = args.length >= 4 ? Integer.parseInt(args[3]) : 0;

                if (distance > 50) {
                    System.out.println("Kejauhan.");
                } else {
                    double sellPrice = calculateTotalPrice(buyPrice, tax, comission) + calculateDeliveryCost(distance);
                    printResult(sellPrice);
                }
            } catch (NumberFormatException e) {
                System.out.println("Argument harus angka.");
            }

        } else {
            System.out.println("Kurang argumen");
        }
    }

    public static int calculateDeliveryCost(int distance) {
        int deliveryCost = 0;

        if (distance >= 5 && distance <= 10) {
            deliveryCost = 10000;
        } else if (distance >= 11 && distance <= 50) {
            deliveryCost = 25000;
        }
        return deliveryCost;
    }

    public static double calculateTotalPrice(int buyPrice, double tax, int comission) {
        return buyPrice + (buyPrice * tax) + comission;
    }

    public static void printResult(double sellPrice) {
        Locale localeID = new Locale("id", "ID");

        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(localeID);

        String formattedSellAmount = currencyFormat.format(sellPrice);

        System.out.println("Harga jual adalah: " + formattedSellAmount);
    }
}